export class LoginPage{
    renderlogin(){
       cy.visit('/#/login')
    }

    login(user,pass){
        cy.get(':nth-child(1) > .form-group > .form-control').type(user)
        cy.get(':nth-child(2) > .form-group > .form-control').type(pass)
        cy.get('.btn').click()  
    }

    validateSuccessLogin(){
        cy.get('.container > .nav > :nth-child(2) > .nav-link').should('have.text',' felixruiz ')
    }

    checkTag(index, tagName){
        cy.get(`.tag-list > :nth-child(${index})`).should('have.text',tagName)
    }

    clickTag(index){
        cy.get(`.tag-list > :nth-child(${index})`).click()
    }

}