export class PostPage{
    renderpost(){
       cy.visit('/#/editor')
    }

    enterArticle(articleString){
        cy.get(':nth-child(1) > .form-group > .form-control').type(articleString)
    }

    enterSubject(subjectString){
        cy.get(':nth-child(2) > .form-group > .form-control').type(subjectString)
    }

    enterMainArticle(articleString){
        cy.get('app-textarea > .form-group > .form-control').type(articleString)
    }

    enterTags(tagsString){
        cy.get(':nth-child(4) > .form-group > .form-control').type(tagsString)
    }

    publish(){
        cy.get('.btn').click()
    }

}