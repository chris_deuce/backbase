export class RegisterPage{
    renderRegister(){
       cy.visit('/#/register')
    }

    createUsername(usernameString){
        cy.get(':nth-child(1) > .form-group > .form-control').type(usernameString)
    }

    enterMail(mailString){
        cy.get(':nth-child(2) > .form-group > .form-control').type(mailString)
    }

    createPass(passString){
        cy.get(':nth-child(3) > .form-group > .form-control').type(passString)
    }

    submit(){
        cy.get('.btn').click()
    }

}

//https://qa-task.backbasecloud.com/#/register