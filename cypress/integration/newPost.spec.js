/// <reference types="cypress" /> ///

import { LandPage } from '../page-objects/land-page';
import { LoginPage } from '../page-objects/login-page';
import { PostPage } from '../page-objects/post-page';

describe('Posting to BBlog actions',()=>{
    const landPage = new LandPage()
    const loginPage = new LoginPage()
    const postPage = new PostPage()
     beforeEach(()=>{
        landPage.navigate()
        loginPage.renderlogin()
        loginPage.login('felixruiz.mendoza@gmail.com','S4ndP4p3r#1')
        loginPage.validateSuccessLogin()
     })

    it ('should post a new article',()=>{
        postPage.renderpost()
        postPage.enterArticle('Error for test automation')
        postPage.enterSubject('Web driver is aging but new contenders are..')
        postPage.enterMainArticle('Beware of the newcomers, even if webdriver is the tool to select as the first option ....')
        postPage.enterTags('Automation, web, cypress, Nowebdriver, locus')
        postPage.publish()
        cy.get('.card-footer > .btn').should('have.text',' Post Comment ')
    })
})

