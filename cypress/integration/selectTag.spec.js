/// <reference types="cypress"/> ///

it ('should redirect to the bblog page App',()=>{
    cy.visit('/',{auth:{username:'candidatex',password:'qa-is-cool'}})
    cy.get('.container > .nav > :nth-child(2) > .nav-link').click()

    cy.get(':nth-child(1) > .form-group > .form-control').type('felixruiz.mendoza@gmail.com')
    cy.get(':nth-child(2) > .form-group > .form-control').type('S4ndP4p3r#1')
    cy.get('.btn').click()
    cy.get('.container > .nav > :nth-child(2) > .nav-link').should('have.text',' felixruiz ')
    cy.get('.tag-list > :nth-child(1)').should('have.text','microchip')
})