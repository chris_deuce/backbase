/// <reference types="cypress"/> ///

import { LandPage } from '../page-objects/land-page';
import { RegisterPage } from '../page-objects/register-page';

describe('Creating an account actions',()=>{
    const landPage = new LandPage()
     const registerPage = new RegisterPage()
     beforeEach(()=>{
       landPage.navigate()
     })
 
    it ('should create a new acount',()=>{
        registerPage.renderRegister()
        registerPage.createUsername('daul23')
        registerPage.enterMail('daul@mail.com')
        registerPage.createPass('deadmaus#1')
        registerPage.submit()
    })
    
 })