/// <reference types="cypress"/> ///

import { LandPage } from '../page-objects/land-page';
import { LoginPage } from '../page-objects/login-page';
import { PostPage } from '../page-objects/post-page';

describe('Posting to BBlog actions',()=>{
   const landPage = new LandPage()
   const loginPage = new LoginPage()
   const postPage = new PostPage()
    beforeEach(()=>{
      landPage.navigate()
      loginPage.renderlogin()
      loginPage.login('felixruiz.mendoza@gmail.com','S4ndP4p3r#1')
      loginPage.validateSuccessLogin()
    })

   it ('should fav an article from a given tag article',()=>{
      loginPage.checkTag(2,'application')
      loginPage.clickTag(2)
    //cy.get('.tag-list > :nth-child(2)').should('have.text','application')
   // cy.get('.tag-list > :nth-child(2)').click()
    cy.get(':nth-child(3) > .article-preview > .article-meta > .btn').click()
   })
   
})